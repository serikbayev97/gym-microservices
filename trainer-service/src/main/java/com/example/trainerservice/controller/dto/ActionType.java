package com.example.trainerservice.controller.dto;

public enum ActionType {
    ADD, DELETE
}
