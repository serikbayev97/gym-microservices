package com.example.trainerservice.controller;

import com.example.trainerservice.controller.dto.ActionDTO;
import com.example.trainerservice.service.TrainerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/trainers")
@RequiredArgsConstructor
public class TrainerController {

    private final TrainerService trainerService;

    @PostMapping
    public ResponseEntity<Void> action(@RequestBody ActionDTO actionDTO) {
        trainerService.trainerWorkload(actionDTO);
        return ResponseEntity.ok().build();
    }

}
