package com.example.trainerservice.client.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TrainerRegistrationRequest {
    private String firstName;
    private String lastName;
    private TrainingType specialization;
}