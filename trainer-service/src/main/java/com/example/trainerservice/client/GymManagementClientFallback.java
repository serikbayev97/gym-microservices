package com.example.trainerservice.client;

import com.example.trainerservice.client.dto.RegistrationResponse;
import com.example.trainerservice.client.dto.TrainerRegistrationRequest;
import org.springframework.stereotype.Component;

@Component
public class GymManagementClientFallback implements GymManagementClient {

    @Override
    public RegistrationResponse register(TrainerRegistrationRequest request) {
        return new RegistrationResponse();
    }

    @Override
    public void removeTrainer(String username) {
    }
}
