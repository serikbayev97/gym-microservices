package com.example.trainerservice.client;

import com.example.trainerservice.client.dto.RegistrationResponse;
import com.example.trainerservice.client.dto.TrainerRegistrationRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "gym-management", fallback = GymManagementClientFallback.class)
public interface GymManagementClient {

    @PostMapping("/api/trainers")
    RegistrationResponse register(@RequestBody TrainerRegistrationRequest request);

    @DeleteMapping("/{username}")
    void removeTrainer(@PathVariable String username);

}
