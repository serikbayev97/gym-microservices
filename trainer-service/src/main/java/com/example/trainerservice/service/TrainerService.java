package com.example.trainerservice.service;

import com.example.trainerservice.client.GymManagementClient;
import com.example.trainerservice.client.dto.RegistrationResponse;
import com.example.trainerservice.client.dto.TrainerRegistrationRequest;
import com.example.trainerservice.client.dto.TrainingType;
import com.example.trainerservice.controller.dto.ActionDTO;
import com.example.trainerservice.controller.dto.ActionType;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.TrainingMessage;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.jms.Queue;
import java.util.Date;
import java.util.Objects;

@Service
@Slf4j
@RequiredArgsConstructor
@Transactional
public class TrainerService {

    private final GymManagementClient client;
    private final InMemoryStructure inMemoryStructure;
    private final JmsTemplate jmsTemplate;
    private final Queue queue;
    public void trainerWorkload(ActionDTO actionDTO) {
        if (actionDTO.getActionType() == ActionType.ADD) {
            log.info("Adding training to trainer!");
            RegistrationResponse registeredTrainer = this.registerTrainer(actionDTO.getFirstName(), actionDTO.getLastName());
            log.info("Trainer: {} successfully registered", registeredTrainer);
            addTrainingToTrainer(registeredTrainer.getUsername(), actionDTO.getTrainingDate(), actionDTO.getTrainingDuration());
            inMemoryStructure.addOperation(actionDTO);
        } else if (actionDTO.getActionType() == ActionType.DELETE) {
            log.info("Deleting trainer");
            client.removeTrainer(actionDTO.getUsername());
            inMemoryStructure.removeOperation(actionDTO);
        }
    }

    public RegistrationResponse registerTrainer(String firstName, String lastName) {
        TrainingType specialization = new TrainingType("XStack");
        return client.register(new TrainerRegistrationRequest(firstName, lastName, specialization));
    }

    public void addTrainingToTrainer(String trainerUsername, Date trainingDate,  Integer duration) {
        if (Objects.isNull(trainerUsername))
            throw new RuntimeException("Trainer username must be not null!");

        TrainingMessage trainingMessage = new TrainingMessage();
        trainingMessage.setTrainerUsername(trainerUsername);
        trainingMessage.setTrainingName("Java/Js/Cloud");
        trainingMessage.setTrainingDate(trainingDate);
        trainingMessage.setDuration(duration);
        log.info("Sending message: {}", trainingMessage);
        jmsTemplate.convertAndSend(queue, trainingMessage);
    }

}
