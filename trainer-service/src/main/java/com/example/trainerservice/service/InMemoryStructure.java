package com.example.trainerservice.service;

import com.example.trainerservice.controller.dto.ActionDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.time.DateFormatUtils;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Slf4j
public class InMemoryStructure {

    private static final Map<String, TrainerSummary> trainerMap = new HashMap<>();

    public void addOperation(ActionDTO actionDTO) {
        TrainerSummary currentSummary = trainerMap.get(actionDTO.getUsername());
        LocalDate localDate = actionDTO.getTrainingDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        log.info("add operation: {}", currentSummary);

        if (currentSummary == null) {
            List<Integer> years = new ArrayList<>();
            years.add(localDate.getYear());

            List<String> months = new ArrayList<>();
            months.add(localDate.getMonth().name());

            TrainerSummary newSummary = TrainerSummary.builder()
                    .username(actionDTO.getUsername())
                    .firstName(actionDTO.getFirstName())
                    .lastName(actionDTO.getLastName())
                    .status(actionDTO.getIsActive())
                    .duration(actionDTO.getTrainingDuration())
                    .years(years)
                    .months(months)
                    .build();


            trainerMap.put(actionDTO.getUsername(), newSummary);
        } else {
            currentSummary.setFirstName(actionDTO.getFirstName());
            currentSummary.setLastName(actionDTO.getLastName());
            currentSummary.setStatus(actionDTO.getIsActive());
            currentSummary.setDuration(currentSummary.getDuration() + actionDTO.getTrainingDuration());
            List<Integer> years = currentSummary.getYears();
            years.add(localDate.getYear());
            currentSummary.setYears(years);
            List<String> months = currentSummary.getMonths();
            months.add(localDate.getMonth().name());
            currentSummary.setMonths(months);
        }
    }

    public void removeOperation(ActionDTO actionDTO) {
        trainerMap.remove(actionDTO.getUsername());
    }

    public Map<String, TrainerSummary> getTrainerMap() {
        return trainerMap;
    }

    public TrainerSummary get(String username) {
        return trainerMap.get(username);
    }


}
