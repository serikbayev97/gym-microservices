package com.example.trainerservice.service;

import lombok.Builder;
import lombok.Data;

import java.util.List;
@Data
@Builder
public class TrainerSummary {
    private String username;
    private String firstName;
    private String lastName;
    private boolean status;
    private List<Integer> years;
    private List<String> months;
    private Integer duration;

}
