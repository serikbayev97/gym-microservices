package com.example.trainerservice.service;

import com.example.trainerservice.controller.dto.ActionDTO;
import com.example.trainerservice.controller.dto.ActionType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class InMemoryStructureTest {

    private InMemoryStructure inMemoryStructure;

    @BeforeEach
    void setUp() {
        inMemoryStructure = new InMemoryStructure();
    }

    @Test
    void testAddOperation() {
        Date someDate = new Date();  // Replace with an actual date
        ActionDTO actionDTO = new ActionDTO("user1", "John", "Doe", true, someDate, 60, ActionType.ADD);

        // Test adding a new entry
        inMemoryStructure.addOperation(actionDTO);
        assertTrue(inMemoryStructure.getTrainerMap().containsKey("user1"));

        // Test updating an existing entry
        ActionDTO updatedDTO = new ActionDTO("user1", "Jane", "Smith", false, someDate, 30, ActionType.ADD);
        inMemoryStructure.addOperation(updatedDTO);
        assertEquals("Jane", inMemoryStructure.get("user1").getFirstName());
        assertFalse(inMemoryStructure.get("user1").isStatus());
    }

    @Test
    void testRemoveOperation() {
        Date someDate = new Date();  // Replace with an actual date
        ActionDTO actionDTO = new ActionDTO("user1", "John", "Doe", true, someDate, 60, ActionType.ADD);

        // Test removing an existing entry
        inMemoryStructure.addOperation(actionDTO);
        assertTrue(inMemoryStructure.getTrainerMap().containsKey("user1"));

        ActionDTO deleteDTO = new ActionDTO("user1", "", "", false, someDate, 0, ActionType.DELETE);
        inMemoryStructure.removeOperation(deleteDTO);
        assertFalse(inMemoryStructure.getTrainerMap().containsKey("user1"));

        // Test removing a non-existing entry
        ActionDTO nonExistingDTO = new ActionDTO("nonexistentUser", "", "", false, someDate, 0, ActionType.DELETE);
        inMemoryStructure.removeOperation(nonExistingDTO);
        assertFalse(inMemoryStructure.getTrainerMap().containsKey("nonexistentUser"));
    }

    // Add more tests as needed to cover additional scenarios and edge cases.
}
