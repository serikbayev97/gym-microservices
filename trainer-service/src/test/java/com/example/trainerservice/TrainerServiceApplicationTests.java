package com.example.trainerservice;

import org.apache.commons.lang.time.DateFormatUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

@SpringBootTest
class TrainerServiceApplicationTests {

    @Test
    void contextLoads() {
        Date date = new Date();
        String formatted = DateFormatUtils.format(date, "yyyy,MM");
        System.out.println("dateformat: " + formatted);
    }

}
