package org.example;

import java.io.Serializable;
import java.util.Date;

public class TrainingMessage implements Serializable {
    private static final long serialVersionUID = 1L;

    private String traineeUsername;
    private String trainerUsername;
    private String trainingName;
    private Date trainingDate;
    private Integer duration;

    public TrainingMessage() {}

    public String getTraineeUsername() {
        return traineeUsername;
    }

    public void setTraineeUsername(String traineeUsername) {
        this.traineeUsername = traineeUsername;
    }

    public String getTrainerUsername() {
        return trainerUsername;
    }

    public void setTrainerUsername(String trainerUsername) {
        this.trainerUsername = trainerUsername;
    }

    public String getTrainingName() {
        return trainingName;
    }

    public void setTrainingName(String trainingName) {
        this.trainingName = trainingName;
    }

    public Date getTrainingDate() {
        return trainingDate;
    }

    public void setTrainingDate(Date trainingDate) {
        this.trainingDate = trainingDate;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "TrainingMessage{" +
                "traineeUsername='" + traineeUsername + '\'' +
                ", trainerUsername='" + trainerUsername + '\'' +
                ", trainingName='" + trainingName + '\'' +
                ", trainingDate=" + trainingDate +
                ", duration=" + duration +
                '}';
    }
}
