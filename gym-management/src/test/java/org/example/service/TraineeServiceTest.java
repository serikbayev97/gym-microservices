package org.example.service;

import org.example.domain.Trainee;
import org.example.domain.User;
import org.example.repository.TraineeRepository;
import org.example.rest.dto.RegistrationResponse;
import org.example.rest.dto.TraineeRegistrationRequest;
import org.example.service.dto.TrainerListDTO;
import org.example.service.util.DomainUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
public class TraineeServiceTest {

    @InjectMocks
    private TraineeService traineeService;

    @Mock
    private TraineeRepository traineeRepository;

    @Mock
    private UserService userService;

    @Mock
    private TrainerService trainerService;

    @Mock
    private PasswordEncoder passwordEncoder;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreate() {
        // Mocking request
        TraineeRegistrationRequest registrationRequest = new TraineeRegistrationRequest("John", "Doe", new Date(), "Address");

        // Mocking userService response
        User existingUser = new User();
        existingUser.setId(1L);
        List<User> existingUsers = Collections.singletonList(existingUser);
        when(userService.getAllByUsername(anyString())).thenReturn(existingUsers);

        // Mocking traineeRepository save
        Trainee savedTrainee = new Trainee();
        savedTrainee.setUser(existingUser);
        when(traineeRepository.save(any(Trainee.class))).thenReturn(savedTrainee);

        String rawPassword = "password123";
        MockedStatic<DomainUtil> utility = Mockito.mockStatic(DomainUtil.class);
        utility.when(DomainUtil::generatePassword).thenReturn(rawPassword);
        String encodedPassword = "encoded-password";
        when(passwordEncoder.encode(rawPassword)).thenReturn(encodedPassword);

        // Perform the test
        RegistrationResponse response = traineeService.create(registrationRequest);
        System.out.println(response);

        // Verify that userService was called
        verify(userService, times(1)).getAllByUsername(anyString());

        // Verify that traineeRepository save was called
        verify(traineeRepository, times(1)).save(any(Trainee.class));

        // Verify that passwordEncoder was called
        verify(passwordEncoder, times(1)).encode(rawPassword);

        // Verify the response
        assertNotNull(response);
        assertEquals(savedTrainee.getUser().getUsername(), response.getUsername());

        // Verify that the password in the response matches the encoded password
        assertEquals(rawPassword, response.getPassword());

    }

    @Test
    public void testUpdateTrainers() {
        // Arrange
        String username = "john_doe";
        List<String> trainerUsernames = new ArrayList<>();
        // Add trainer usernames to the list
        Trainee trainee = new Trainee(/* initialize trainee data */);
        when(traineeRepository.findOneByUsername(username)).thenReturn(Optional.of(trainee));
        when(trainerService.findAllByUsernameList(trainerUsernames)).thenReturn(new HashSet<>());
        when(traineeRepository.save(any(Trainee.class))).thenReturn(trainee);

        // Act
        List<TrainerListDTO> updatedTrainers = traineeService.updateTrainers(username, trainerUsernames);

        // Assert
        assertNotNull(updatedTrainers);
        // Add more assertions based on your specific update logic
    }

    @Test
    public void testDeleteTraineeById() {
        // Arrange
        Long traineeId = 1L;

        // Act
        traineeService.deleteById(traineeId);

        // Assert
        verify(traineeRepository, times(1)).deleteById(traineeId);
    }

    @Test
    public void testGetTraineeById() {
        // Arrange
        Long traineeId = 1L;
        Trainee trainee = new Trainee(/* initialize trainee data */);
        when(traineeRepository.findById(traineeId)).thenReturn(Optional.of(trainee));

        // Act
        Trainee retrievedTrainee = traineeService.getById(traineeId);

        // Assert
        assertNotNull(retrievedTrainee);
        // Add more assertions based on your specific retrieval logic
    }


    @Test
    public void testGetTraineeByUsername() {
        // Arrange
        String username = "john_doe";
        Trainee trainee = new Trainee(/* initialize trainee data */);
        when(traineeRepository.findOneByUsername(username)).thenReturn(Optional.of(trainee));

        // Act
        Trainee retrievedTrainee = traineeService.getByUsername(username);

        // Assert
        assertNotNull(retrievedTrainee);
        // Add more assertions based on your specific retrieval logic
    }

    @Test
    public void testDeleteTraineeByUsername() {
        // Arrange
        String username = "john_doe";

        // Act
        traineeService.deleteByUsername(username);

        // Assert
        verify(traineeRepository, times(1)).deleteByTraineeUsername(username);
    }

    // Add more test methods for other service methods as needed
}
