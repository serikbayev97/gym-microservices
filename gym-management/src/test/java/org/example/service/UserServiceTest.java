package org.example.service;

import org.example.domain.User;
import org.example.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

public class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testIsAuthenticateWithValidCredentials() {
        // Arrange
        String username = "john_doe";
        String password = "password";

        when(userRepository.existsByUsernameAndPasswordEquals(username, password)).thenReturn(true);

        // Act
        boolean isAuthenticated = userService.isAuthenticate(username, password);

        // Assert
        assertTrue(isAuthenticated);
    }

    @Test
    public void testIsAuthenticateWithInvalidCredentials() {
        // Arrange
        String username = "john_doe";
        String password = "wrong_password";

        when(userRepository.existsByUsernameAndPasswordEquals(username, password)).thenReturn(false);

        // Act
        boolean isAuthenticated = userService.isAuthenticate(username, password);

        // Assert
        assertFalse(isAuthenticated);
    }

    @Test
    public void testChangePasswordWithCorrectCurrentPassword() {
        // Arrange
        String username = "john_doe";
        String currentPassword = "current_password";
        String newPassword = "new_password";
        User user = new User();
        user.setUsername(username);
        user.setPassword(currentPassword);

        when(userRepository.findByUsername(username)).thenReturn(Optional.of(user));

        // Act
        assertDoesNotThrow(() -> userService.changePassword(username, currentPassword, newPassword));

        // Assert
        assertEquals(newPassword, user.getPassword());
    }

    @Test
    public void testChangePasswordWithIncorrectCurrentPassword() {
        // Arrange
        String username = "john_doe";
        String currentPassword = "current_password";
        String newPassword = "new_password";
        User user = new User();
        user.setUsername(username);
        user.setPassword("wrong_password");

        when(userRepository.findByUsername(username)).thenReturn(Optional.of(user));

        // Act and Assert
        assertThrows(RuntimeException.class, () -> userService.changePassword(username, currentPassword, newPassword));
    }

    @Test
    public void testChangePasswordWithNonExistentUser() {
        // Arrange
        String username = "non_existent_user";
        String currentPassword = "current_password";
        String newPassword = "new_password";

        when(userRepository.findByUsername(username)).thenReturn(Optional.empty());

        // Act and Assert
        assertThrows(EntityNotFoundException.class, () -> userService.changePassword(username, currentPassword, newPassword));
    }

    // Add more test methods for other service methods as needed
}
