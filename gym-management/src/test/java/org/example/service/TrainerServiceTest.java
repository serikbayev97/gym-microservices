package org.example.service;

import org.example.domain.Trainer;
import org.example.domain.User;
import org.example.repository.TrainerRepository;
import org.example.rest.dto.RegistrationResponse;
import org.example.rest.dto.TrainerRegistrationRequest;
import org.example.service.dto.TrainerListDTO;
import org.example.service.util.DomainUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TrainerServiceTest {

    @InjectMocks
    private TrainerService trainerService;

    @Mock
    private TrainerRepository trainerRepository;

    @Mock
    private UserService userService;

    @Mock
    private PasswordEncoder passwordEncoder;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreate() {
        // Mocking request
        TrainerRegistrationRequest registrationRequest = new TrainerRegistrationRequest("John", "Doe", null);

        // Mocking userService response
        User existingUser = new User();
        existingUser.setId(1L);
        List<User> existingUsers = Collections.singletonList(existingUser);
        when(userService.getAllByUsername(anyString())).thenReturn(existingUsers);

        // Mocking trainerRepository save
        Trainer savedTrainer = new Trainer();
        savedTrainer.setUser(existingUser);
        when(trainerRepository.save(any(Trainer.class))).thenReturn(savedTrainer);

        String rawPassword = "password123";
        MockedStatic<DomainUtil> utility = Mockito.mockStatic(DomainUtil.class);
        utility.when(DomainUtil::generatePassword).thenReturn(rawPassword);
        String encodedPassword = "encoded-password";
        when(passwordEncoder.encode(rawPassword)).thenReturn(encodedPassword);

        // Perform the test
        RegistrationResponse response = trainerService.create(registrationRequest);

        // Verify that userService was called
        verify(userService, times(1)).getAllByUsername(anyString());

        // Verify that trainerRepository save was called
        verify(trainerRepository, times(1)).save(any(Trainer.class));

        // Verify that passwordEncoder was called
        verify(passwordEncoder, times(1)).encode(rawPassword);

        // Verify the response
        assertNotNull(response);
        assertEquals(savedTrainer.getUser().getUsername(), response.getUsername());

        // Verify that the password in the response matches the encoded password
        assertEquals(rawPassword, response.getPassword());
    }

    @Test
    public void testSelectTrainer() {
        // Arrange
        Long trainerId = 1L;
        Trainer trainer = new Trainer(/* initialize trainer data */);
        when(trainerRepository.findById(trainerId)).thenReturn(Optional.of(trainer));

        // Act
        Trainer selectedTrainer = trainerService.select(trainerId);

        // Assert
        assertNotNull(selectedTrainer);
        // Add more assertions based on your specific selection logic
    }

    @Test
    public void testGetTrainerById() {
        // Arrange
        Long trainerId = 1L;
        Trainer trainer = new Trainer(/* initialize trainer data */);
        when(trainerRepository.findById(trainerId)).thenReturn(Optional.of(trainer));

        // Act
        Trainer retrievedTrainer = trainerService.getById(trainerId);

        // Assert
        assertNotNull(retrievedTrainer);
        // Add more assertions based on your specific retrieval logic
    }

    @Test
    public void testGetAllNotAssignedActiveTrainers() {
        // Arrange
        String username = "john_doe";
        List<Trainer> trainers = new ArrayList<>();
        // Populate the trainers list with sample data
        when(trainerRepository.findAll()).thenReturn(trainers);

        // Act
        List<TrainerListDTO> notAssignedActiveTrainers = trainerService.getAllNotAssignedActiveTrainers(username);

        // Assert
        assertNotNull(notAssignedActiveTrainers);
        // Add more assertions based on your specific retrieval logic
    }

    @Test
    public void testFindAllByUsernameList() {
        // Arrange
        List<String> usernames = new ArrayList<>();
        // Populate the usernames list with sample data
        Set<Trainer> trainers = new HashSet<>();
        // Populate the trainers set with sample data
        when(trainerRepository.findAllByUsernamesIn(usernames)).thenReturn(trainers);

        // Act
        Set<Trainer> retrievedTrainers = trainerService.findAllByUsernameList(usernames);

        // Assert
        assertNotNull(retrievedTrainers);
        // Add more assertions based on your specific retrieval logic
    }
    @Test
    public void testGetTrainerByUsername() {
        // Arrange
        String username = "john_doe";
        Trainer trainer = new Trainer(/* initialize trainer data */);
        when(trainerRepository.findOneByUsername(username)).thenReturn(Optional.of(trainer));

        // Act
        Trainer retrievedTrainer = trainerService.getByUsername(username);

        // Assert
        assertNotNull(retrievedTrainer);
        // Add more assertions based on your specific retrieval logic
    }

    // Add more test methods for other service methods as needed
}
