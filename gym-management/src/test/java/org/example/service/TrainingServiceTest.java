package org.example.service;

import org.example.domain.Trainee;
import org.example.domain.Trainer;
import org.example.domain.Training;
import org.example.repository.TrainingRepository;
import org.example.rest.dto.TrainingRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TrainingServiceTest {

    @InjectMocks
    private TrainingService trainingService;

    @Mock
    private TrainingRepository trainingRepository;

    @Mock
    private TrainerService trainerService;

    @Mock
    private TraineeService traineeService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAddTraining() {
        // Arrange
        TrainingRequest request = new TrainingRequest(/* initialize request data */);
        Trainer trainer = new Trainer(/* initialize trainer data */);
        Trainee trainee = new Trainee(/* initialize trainee data */);

        when(trainerService.getByUsername(request.getTrainerUsername())).thenReturn(trainer);
        when(traineeService.getByUsername(request.getTraineeUsername())).thenReturn(trainee);

        // Act
        trainingService.addTraining(request);

        // Assert
        verify(trainingRepository, times(1)).save(any(Training.class));
        // Add more assertions based on your specific addTraining logic
    }

    // Add more test methods for other service methods as needed
}
