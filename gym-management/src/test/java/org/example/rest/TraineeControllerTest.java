package org.example.rest;

import org.example.domain.Trainee;
import org.example.domain.User;
import org.example.rest.dto.RegistrationResponse;
import org.example.rest.dto.TraineeRegistrationRequest;
import org.example.rest.dto.TraineeRequest;
import org.example.rest.dto.TrainingCriteria;
import org.example.service.TraineeService;
import org.example.service.dto.TETrainingListDTO;
import org.example.service.dto.TraineeDTO;
import org.example.service.dto.TrainerListDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TraineeControllerTest {

    @InjectMocks
    private TraineeController traineeController;

    @Mock
    private TraineeService traineeService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetByUsername() {
        // Arrange
        String username = "john_doe";
        Trainee trainee = new Trainee(new User());
        when(traineeService.getByUsername(username)).thenReturn(trainee);

        // Act
        ResponseEntity<TraineeDTO> response = traineeController.getByUsername(username);

        // Assert
        verify(traineeService, times(1)).getByUsername(username);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetTrainingsByCriteria() {
        // Arrange
        TrainingCriteria criteria = new TrainingCriteria();
        List<TETrainingListDTO> list = new ArrayList<>();
        when(traineeService.getTrainingsByCriteria(criteria)).thenReturn(list);

        // Act
        ResponseEntity<List<TETrainingListDTO>> response = traineeController.getTrainings(criteria);

        // Assert
        verify(traineeService, times(1)).getTrainingsByCriteria(criteria);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }


    @Test
    public void testUpdateTrainers() {
        // Arrange
        String username = "john_doe";
        List<String> trainers = new ArrayList<>();
        List<TrainerListDTO> trainerListDTOS = new ArrayList<>();
        when(traineeService.updateTrainers(username, trainers)).thenReturn(trainerListDTOS);

        // Act
        ResponseEntity<List<TrainerListDTO>> response = traineeController.updateTrainers(username, trainers);

        // Assert
        verify(traineeService, times(1)).updateTrainers(username, trainers);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testDeleteByUsername() {
        // Arrange
        String username = "john_doe";

        // Act
        ResponseEntity<Void> response = traineeController.deleteByUsername(username);

        // Assert
        verify(traineeService, times(1)).deleteByUsername(username);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testUpdateActive() {
        // Arrange
        String username = "john_doe";
        boolean activated = true;

        // Act
        ResponseEntity<Void> response = traineeController.updateActive(username, activated);

        // Assert
        verify(traineeService, times(1)).updateActivation(username, activated);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }
}
