package org.example.rest;

import org.example.domain.Trainer;
import org.example.domain.TrainingType;
import org.example.domain.User;
import org.example.rest.dto.TrainingCriteria;
import org.example.service.TrainerService;
import org.example.service.dto.TRTrainingListDTO;
import org.example.service.dto.TrainerDTO;
import org.example.service.dto.TrainerListDTO;
import org.example.service.dto.TrainerRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TrainerControllerTest {

    @InjectMocks
    private TrainerController trainerController;

    @Mock
    private TrainerService trainerService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetByUsername() {
        // Arrange
        String username = "john_doe";
        Trainer trainer = new Trainer(new User(), new TrainingType());
        when(trainerService.getByUsername(username)).thenReturn(trainer);

        // Act
        ResponseEntity<TrainerDTO> response = trainerController.getByUsername(username);

        // Assert
        verify(trainerService, times(1)).getByUsername(username);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testUpdateTrainer() {
        // Arrange
        String username = "john_doe";
        TrainerRequest trainerRequest = new TrainerRequest();
        TrainerDTO trainerDTO = new TrainerDTO(new Trainer(new User(), new TrainingType()));
        when(trainerService.update(username, trainerRequest)).thenReturn(trainerDTO);

        // Act
        ResponseEntity<TrainerDTO> response = trainerController.updateTrainer(username, trainerRequest);

        // Assert
        verify(trainerService, times(1)).update(username, trainerRequest);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetAllNotAssignedActiveTrainers() {
        // Arrange
        String username = "john_doe";
        List<TrainerListDTO> trainerListDTOS = new ArrayList<>();
        when(trainerService.getAllNotAssignedActiveTrainers(username)).thenReturn(trainerListDTOS);

        // Act
        ResponseEntity<List<TrainerListDTO>> response = trainerController.getAllNotAssignedActiveTrainers(username);

        // Assert
        verify(trainerService, times(1)).getAllNotAssignedActiveTrainers(username);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetTrainingsByCriteria() {
        // Arrange
        TrainingCriteria criteria = new TrainingCriteria();
        List<TRTrainingListDTO> list = new ArrayList<>();
        when(trainerService.getTrainingsByCriteria(criteria)).thenReturn(list);

        // Act
        ResponseEntity<List<TRTrainingListDTO>> response = trainerController.getTrainings(criteria);

        // Assert
        verify(trainerService, times(1)).getTrainingsByCriteria(criteria);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testUpdateActive() {
        // Arrange
        String username = "john_doe";
        boolean activated = true;

        // Act
        ResponseEntity<Void> response = trainerController.updateActive(username, activated);

        // Assert
        verify(trainerService, times(1)).updateActivation(username, activated);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }
}
