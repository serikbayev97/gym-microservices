package org.example.rest;

import org.example.rest.dto.ChangePasswordRequest;
import org.example.rest.dto.LoginRequest;
import org.example.security.JWTFilter;
import org.example.security.TokenProvider;
import org.example.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class LoginControllerTest {

    private LoginController loginController;

    @Mock
    private UserService userService;

    @Mock
    private TokenProvider tokenProvider;

    @Mock
    private AuthenticationManagerBuilder authenticationManagerBuilder;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        loginController = new LoginController(userService, tokenProvider, authenticationManagerBuilder);
    }

    @Test
    public void testChangePassword() {
        // Mocking request
        ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest("username", "password", "newPassword");

        // Perform the test
        ResponseEntity<Void> responseEntity = loginController.changePassword(changePasswordRequest);

        // Verify that the expected method was called
        verify(userService, times(1)).changePassword("username", "password", "newPassword");

        // Assert the response
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }
}
