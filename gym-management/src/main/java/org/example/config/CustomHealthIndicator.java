package org.example.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.sql.DataSource;

@Component
@Slf4j
public class CustomHealthIndicator implements HealthIndicator {

    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    DataSource dataSource;
    @Override
    public Health health() {
        try {
            // Attempt to create an EntityManager to check the database connection
            entityManagerFactory.createEntityManager();
            return Health.up().build();
        } catch (Exception e) {
            // If an exception occurs, mark the health as down with an error message
            return Health.down().withDetail("Database", "Connection failed: " + e.getMessage()).build();
        }
    }
}
