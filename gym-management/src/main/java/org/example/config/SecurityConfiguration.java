package org.example.config;

import lombok.RequiredArgsConstructor;
import org.example.security.JWTConfigurer;
import org.example.security.TokenProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableMethodSecurity(securedEnabled = true)
@RequiredArgsConstructor
public class SecurityConfiguration {

    private final TokenProvider tokenProvider;

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {

        http
                .cors()
                .and()
                .csrf()
                .disable()
                .authorizeRequests((requests) -> requests
                        .antMatchers(HttpMethod.POST, "/api/trainees","/api/trainers").permitAll()
                        .antMatchers("/error").permitAll()
                        .antMatchers("/api/login").permitAll()
                        .anyRequest().authenticated()
                )
                .httpBasic()
                .and()
                .apply(jwtConfigurer());

        return http.build();
    }

    private JWTConfigurer jwtConfigurer() {
        return new JWTConfigurer(tokenProvider);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
