package org.example.criteria;

import org.example.domain.Trainee;
import org.example.domain.Trainer;
import org.example.domain.Training;
import org.example.domain.User;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import java.util.Date;

public class TrainerSpecifications {

    public static Specification<Trainer> hasUsername(String username) {
        return (root, query, criteriaBuilder) -> {
            Join<Trainer, User> userJoin = root.join("user", JoinType.INNER);
            return criteriaBuilder.equal(userJoin.get("username"), username);
        };
    }

    public static Specification<Trainer> hasTraineeName(String traineeName) {
        return (root, query, criteriaBuilder) -> {
            Join<Trainer, Trainee> traineeJoin = root.join("trainers", JoinType.INNER);
            return criteriaBuilder.equal(traineeJoin.get("name"), traineeName);
        };
    }

    public static Specification<Trainer> hasTraining(String trainingName) {
        return (root, query, criteriaBuilder) -> {
            Join<Trainer, Training> trainingJoin = root.join("trainings", JoinType.INNER);
            return criteriaBuilder.equal(trainingJoin.get("name"), trainingName);
        };
    }

    public static Specification<Trainer> greaterThan(Date date) {
        return (root, query, criteriaBuilder) -> {
            Join<Trainer, Training> trainingJoin = root.join("trainings", JoinType.INNER);
            return criteriaBuilder.greaterThan(trainingJoin.get("date"), date);
        };
    }


    public static Specification<Trainer> lessThan(Date date) {
        return (root, query, criteriaBuilder) -> {
            Join<Trainer, Training> trainingJoin = root.join("trainings", JoinType.INNER);
            return criteriaBuilder.lessThan(trainingJoin.get("date"), date);
        };
    }

    public static Specification<Trainer> equalTrainingId(Long trainingId) {
        return (root, query, criteriaBuilder) -> {
            Join<Trainer, Training> trainingJoin = root.join("trainings", JoinType.INNER);
            return criteriaBuilder.equal(trainingJoin.get("id"), trainingId);
        };
    }
}
