package org.example.criteria;

import org.example.domain.Trainee;
import org.example.domain.Trainer;
import org.example.domain.Training;
import org.example.domain.User;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import java.util.Date;

public class TraineeSpecifications {


    public static Specification<Trainee> hasUsername(String username) {
        return (root, query, criteriaBuilder) -> {
            Join<Trainee, User> userJoin = root.join("user", JoinType.INNER);
            return criteriaBuilder.equal(userJoin.get("username"), username);
        };
    }

    public static Specification<Trainee> hasTraining(String trainingName) {
        return (root, query, criteriaBuilder) -> {
            Join<Trainee, Training> trainingJoin = root.join("trainings", JoinType.INNER);
            return criteriaBuilder.equal(trainingJoin.get("name"), trainingName);
        };
    }

    public static Specification<Trainee> hasTrainerName(String trainerName) {
        return (root, query, criteriaBuilder) -> {
            Join<Trainee, Trainer> trainerJoin = root.join("trainers", JoinType.INNER);
            return criteriaBuilder.equal(trainerJoin.get("name"), trainerName);
        };
    }

    public static Specification<Trainee> greaterThan(Date date) {
        return (root, query, criteriaBuilder) -> {
            Join<Trainee, Training> trainingJoin = root.join("trainings", JoinType.INNER);
            return criteriaBuilder.greaterThan(trainingJoin.get("date"), date);
        };
    }


    public static Specification<Trainee> lessThan(Date date) {
        return (root, query, criteriaBuilder) -> {
            Join<Trainee, Training> trainingJoin = root.join("trainings", JoinType.INNER);
            return criteriaBuilder.lessThan(trainingJoin.get("date"), date);
        };
    }

    public static Specification<Trainee> equalTrainingId(Long trainingId) {
        return (root, query, criteriaBuilder) -> {
            Join<Trainee, Training> trainingJoin = root.join("trainings", JoinType.INNER);
            return criteriaBuilder.equal(trainingJoin.get("id"), trainingId);
        };
    }
}
