package org.example.listener;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.TrainingMessage;
import org.example.domain.Trainer;
import org.example.service.TrainerService;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Slf4j
@RequiredArgsConstructor
public class TrainerListener {

    private final TrainerService trainerService;
    @JmsListener(destination = "trainerQueue")
    @Transactional
    public void receiveMessage(TrainingMessage message) {
        log.info("Receive message: {}", message);
        Trainer trainer = trainerService.getByUsername(message.getTrainerUsername());
        trainerService.updateTrainings(trainer, message);
    }
}
