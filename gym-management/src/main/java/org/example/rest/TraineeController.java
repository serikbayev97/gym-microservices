package org.example.rest;

import io.micrometer.core.annotation.Timed;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.domain.Trainee;
import org.example.domain.User;
import org.example.rest.dto.RegistrationResponse;
import org.example.rest.dto.TraineeRegistrationRequest;
import org.example.rest.dto.TraineeRequest;
import org.example.rest.dto.TrainingCriteria;
import org.example.service.TraineeService;
import org.example.service.dto.TETrainingListDTO;
import org.example.service.dto.TraineeDTO;
import org.example.service.dto.TrainerListDTO;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/trainees")
@RequiredArgsConstructor
@Slf4j
public class TraineeController {

    private final TraineeService traineeService;


    @Timed(value = "trainee.register.time", description = "Time taken to register trainee")
    @PostMapping
    public ResponseEntity<RegistrationResponse> register(@RequestBody TraineeRegistrationRequest request,
                                                         UriComponentsBuilder builder) {
        var response = traineeService.create(request);

        return ResponseEntity.created(builder
                        .path("/api/trainees/{username}")
                        .build(Map.of("username", response.getUsername())))
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }
    @Timed(value = "trainee.username.time", description = "Time taken to get trainee by username")
    @GetMapping("/{username}")
    public ResponseEntity<TraineeDTO> getByUsername(@PathVariable String username) {
        Trainee trainee = traineeService.getByUsername(username);
        return ResponseEntity.ok(new TraineeDTO(trainee));
    }

    //
    @GetMapping("/by-criteria/trainings")
    public ResponseEntity<List<TETrainingListDTO>> getTrainings(@RequestBody TrainingCriteria criteria) {
        List<TETrainingListDTO> list = traineeService.getTrainingsByCriteria(criteria);
        return ResponseEntity.ok(list);
    }

    //
    @PutMapping("/{username}")
    public ResponseEntity<TraineeDTO> updateTrainee(@PathVariable String username,
                                                    @RequestBody TraineeRequest traineeRequest) {
        TraineeDTO traineeDTO = traineeService.updateByUsername(username, traineeRequest);
        return ResponseEntity.ok(traineeDTO);
    }

    //
    @PutMapping("/{username}/trainees")
    public ResponseEntity<List<TrainerListDTO>> updateTrainers(@PathVariable String username,
                                                         @RequestBody List<String> trainers) {
        List<TrainerListDTO> trainerListDTOS = traineeService.updateTrainers(username, trainers);
        return ResponseEntity.ok(trainerListDTOS);
    }

    //
    @DeleteMapping("/{username}")
    public ResponseEntity<Void> deleteByUsername(@PathVariable String username) {
        traineeService.deleteByUsername(username);
        return ResponseEntity.ok().build();
    }

    //
    @PatchMapping("/{username}")
    public ResponseEntity<Void> updateActive(@PathVariable String username,
                                             @RequestParam boolean activated) {
        traineeService.updateActivation(username, activated);
        return ResponseEntity.ok().build();
    }


    public Trainee mapToTrainee(TraineeRegistrationRequest request) {
        User user = new User();
        user.setFirstName(request.getFirstName());
        user.setLastName(request.getLastName());

        return new Trainee(request.getDateOfBirth(), request.getAddress(), user);
    }

}
