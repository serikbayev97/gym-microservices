package org.example.rest;

import io.micrometer.core.annotation.Timed;
import lombok.RequiredArgsConstructor;
import org.example.domain.TrainingType;
import org.example.repository.TrainingTypeRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/trainingTypes")
@RequiredArgsConstructor
public class TrainingTypeController {

    private final TrainingTypeRepository trainingTypeRepository;
    @Timed(value = "training-type.time", description = "Time taken to get training type list")
    @GetMapping
    public ResponseEntity<List<TrainingType>> getAll() {
        return ResponseEntity.ok(trainingTypeRepository.findAll());
    }
}
