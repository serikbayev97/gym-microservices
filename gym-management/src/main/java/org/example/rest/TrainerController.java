package org.example.rest;

import io.micrometer.core.annotation.Timed;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.domain.Trainer;
import org.example.domain.User;
import org.example.rest.dto.RegistrationResponse;
import org.example.rest.dto.TrainerRegistrationRequest;
import org.example.rest.dto.TrainingCriteria;
import org.example.service.TrainerService;
import org.example.service.dto.TRTrainingListDTO;
import org.example.service.dto.TrainerDTO;
import org.example.service.dto.TrainerListDTO;
import org.example.service.dto.TrainerRequest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/trainers")
@RequiredArgsConstructor
@Slf4j
public class TrainerController {
    
    private final TrainerService trainerService;

    @Timed(value = "trainer.register.time", description = "Time taken to register trainer")
    @PostMapping
    public ResponseEntity<RegistrationResponse> register(@RequestBody TrainerRegistrationRequest request,
                                                         UriComponentsBuilder builder) {
        var response = trainerService.create(request);

        return ResponseEntity.created(builder
                        .path("/api/trainers/{username}")
                        .build(Map.of("username", response.getUsername())))
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @Timed(value = "trainer.username.time", description = "Time taken to get trainer by username")
    @GetMapping("/{username}")
    public ResponseEntity<TrainerDTO> getByUsername(@PathVariable String username) {
        Trainer trainer  = trainerService.getByUsername(username);
        return ResponseEntity.ok(new TrainerDTO(trainer));
    }

    //
    @PutMapping("/{username}")
    public ResponseEntity<TrainerDTO> updateTrainer(@PathVariable String username,
                                                    @RequestBody TrainerRequest trainerRequest) {
        TrainerDTO trainerDTO = trainerService.update(username, trainerRequest);
        return ResponseEntity.ok(trainerDTO);
    }

    //
    @GetMapping("/{username}/not-assigned")
    public ResponseEntity<List<TrainerListDTO>> getAllNotAssignedActiveTrainers(@PathVariable String username) {
        List<TrainerListDTO> allNotAssignedActiveTrainers = trainerService.getAllNotAssignedActiveTrainers(username);
        return ResponseEntity.ok(allNotAssignedActiveTrainers);
    }

    //
    @GetMapping("/by-criteria/trainings")
    public ResponseEntity<List<TRTrainingListDTO>> getTrainings(@RequestBody TrainingCriteria criteria) {
        List<TRTrainingListDTO> list = trainerService.getTrainingsByCriteria(criteria);
        return ResponseEntity.ok(list);
    }

    //
    @PatchMapping("/{username}")
    public ResponseEntity<Void> updateActive(@PathVariable String username,
                                             @RequestParam boolean activated) {
        trainerService.updateActivation(username, activated);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{username}")
    public ResponseEntity<Void> removeTrainer(@PathVariable String username) {
        trainerService.deleteByUsername(username);
        return ResponseEntity.ok().build();
    }

    private Trainer mapToTrainer(TrainerRegistrationRequest request) {
        User user = new User();
        user.setFirstName(request.getFirstName());
        user.setLastName(request.getLastName());

        return new Trainer(user, request.getSpecialization());
    }
}
