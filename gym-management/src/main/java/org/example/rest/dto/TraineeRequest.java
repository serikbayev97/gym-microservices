package org.example.rest.dto;

import lombok.Data;

import java.util.Date;

@Data
public class TraineeRequest {
    private String firstName;
    private String lastName;
    private Date dateOfBirth;
    private String address;
    private boolean isActive;
}
