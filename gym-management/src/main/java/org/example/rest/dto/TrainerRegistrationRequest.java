package org.example.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.example.domain.TrainingType;

@Data
@AllArgsConstructor
public class TrainerRegistrationRequest {
    private String firstName;
    private String lastName;
    private TrainingType specialization;
}
