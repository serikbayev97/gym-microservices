package org.example.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

import java.util.Date;

@Data
@AllArgsConstructor
public class TraineeRegistrationRequest {
    @NonNull
    private String firstName;
    @NonNull
    private String lastName;
    private Date dateOfBirth;
    private String address;
}
