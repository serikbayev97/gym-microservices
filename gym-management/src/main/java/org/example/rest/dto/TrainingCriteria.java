package org.example.rest.dto;

import lombok.Data;
import org.example.domain.TrainingType;

import java.util.Date;

@Data
public class TrainingCriteria {
    private String username;
    private Date from;
    private Date to;
    private String trainerName;
    private String traineeName;
    private TrainingType trainingType;
}
