package org.example.rest.dto;

import lombok.Data;
import org.example.domain.TrainingType;

@Data
public class TrainerResponse {
    private String username;
    private String firstName;
    private String lastName;
    private TrainingType specialization;
}
