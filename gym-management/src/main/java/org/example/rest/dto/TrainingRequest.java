package org.example.rest.dto;

import lombok.Data;

import java.util.Date;

@Data
public class TrainingRequest {
    private String traineeUsername;
    private String trainerUsername;
    private String trainingName;
    private Date trainingDate;
    private Integer duration;

}
