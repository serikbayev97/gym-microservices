package org.example.service.dto;

import lombok.Data;
import org.example.domain.Training;
import org.example.domain.TrainingType;

import java.util.Date;

@Data
public class TRTrainingListDTO {
    private String name;
    private Date date;
    private TrainingType trainingType;
    private Integer duration;
    private String trainerName;

    public TRTrainingListDTO(Training training, String trainerName) {
        this.name = training.getName();
        this.date = training.getDate();
        this.trainingType = training.getTrainingType();
        this.duration = training.getDuration();
        this.trainerName = trainerName;
    }
}
