package org.example.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TraineeUserDTO {
    private String firstName;
    private String lastName;
    private boolean isActive;
}
