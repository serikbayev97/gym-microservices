package org.example.service.dto;

import lombok.Data;
import org.example.domain.Trainer;
import org.example.domain.TrainingType;

@Data
public class TrainerListDTO {
    private String username;
    private String firstName;
    private String lastName;
    private TrainingType trainingType;

    public TrainerListDTO(Trainer trainer) {
        this.username = trainer.getUser().getUsername();
        this.firstName = trainer.getUser().getFirstName();
        this.lastName = trainer.getUser().getLastName();
        this.trainingType = trainer.getSpecialization();
    }
}
