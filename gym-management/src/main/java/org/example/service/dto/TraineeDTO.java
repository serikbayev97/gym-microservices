package org.example.service.dto;

import lombok.Data;
import org.example.domain.Trainee;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class TraineeDTO {
    private String firstName;
    private String lastName;
    private Date dateOfBirth;
    private String address;
    private boolean isActive;
    private List<TrainerListDTO> trainers;

    public TraineeDTO(Trainee trainee) {
        this.firstName = trainee.getUser().getFirstName();
        this.lastName = trainee.getUser().getLastName();
        this.dateOfBirth = trainee.getDateOfBirth();
        this.address = trainee.getAddress();
        this.trainers = trainee.getTrainers().stream().map(TrainerListDTO::new).collect(Collectors.toList());
    }

}
