package org.example.service.dto;

import lombok.Data;
import org.example.domain.Trainer;
import org.example.domain.TrainingType;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class TrainerDTO {
    private String firstName;
    private String lastName;
    private TrainingType specialization;
    private boolean isActive;
    private List<TraineeListDTO> trainees;

    public TrainerDTO(Trainer trainer) {
        this.firstName = trainer.getUser().getFirstName();
        this.lastName = trainer.getUser().getLastName();
        this.specialization = trainer.getSpecialization();
        this.isActive = trainer.getUser().isActive();
        this.trainees = trainer.getTrainees().stream().map(TraineeListDTO::new).collect(Collectors.toList());
    }
}