package org.example.service.dto;

import lombok.Data;
import org.example.domain.Training;
import org.example.domain.TrainingType;

import java.util.Date;

@Data
public class TETrainingListDTO {
    private String name;
    private Date date;
    private TrainingType trainingType;
    private Integer duration;
    private String traineeName;

    public TETrainingListDTO(Training training, String traineeName) {
        this.name = training.getName();
        this.date = training.getDate();
        this.trainingType = training.getTrainingType();
        this.duration = training.getDuration();
        this.traineeName = traineeName;
    }
}
