package org.example.service.dto;

import lombok.Data;
import org.example.domain.Trainee;

@Data
public class TraineeListDTO {
    private String username;
    private String firstName;
    private String lastName;

    public TraineeListDTO(Trainee trainee) {
        this.username = trainee.getUser().getUsername();
        this.firstName = trainee.getUser().getFirstName();
        this.lastName = trainee.getUser().getLastName();
    }
}
