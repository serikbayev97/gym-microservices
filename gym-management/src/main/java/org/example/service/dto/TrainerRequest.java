package org.example.service.dto;

import lombok.Data;
import org.example.domain.TrainingType;

import java.util.List;

@Data
public class TrainerRequest {
    private String firstName;
    private String lastName;
    private TrainingType specialization;
    private boolean isActive;
}
