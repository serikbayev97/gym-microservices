package org.example.service;

import lombok.extern.slf4j.Slf4j;
import org.example.domain.User;
import org.example.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@Slf4j
public class UserService {
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public boolean isAuthenticate(String username, String password) {
        return userRepository.existsByUsernameAndPasswordEquals(username, password);
    }

    public List<User> getAllByUsername(String username) {
        return userRepository.findAllByUsernameLike(username);
    }

    public void changePassword(String username, String currentPassword, String newPassword) {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Username with username [%s] was not found", username)));

        if (user.getPassword().equals(currentPassword)) {
            log.info("Password is matched");
            user.setPassword(newPassword);
            userRepository.save(user);
        } else {
            throw new RuntimeException("Password is not correct");
        }
    }
}
