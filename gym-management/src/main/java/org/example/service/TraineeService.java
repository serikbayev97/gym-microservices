package org.example.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.criteria.TraineeSpecifications;
import org.example.domain.Trainee;
import org.example.domain.Trainer;
import org.example.domain.User;
import org.example.repository.TraineeRepository;
import org.example.rest.dto.RegistrationResponse;
import org.example.rest.dto.TraineeRegistrationRequest;
import org.example.rest.dto.TraineeRequest;
import org.example.rest.dto.TrainingCriteria;
import org.example.service.dto.TETrainingListDTO;
import org.example.service.dto.TraineeDTO;
import org.example.service.dto.TrainerListDTO;
import org.example.service.util.DomainUtil;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class TraineeService {
    private final PasswordEncoder encoder;
    private final TraineeRepository traineeRepository;
    private final UserService userService;
    private final TrainerService trainerService;
    private final DomainUtil domainUtil;

    public RegistrationResponse create(TraineeRegistrationRequest request) {

        log.info("Trainee registration request {}", request);
        int serialNumber = 0;

        Long lastId = userService.getAllByUsername(String.join(".", request.getFirstName(), request.getLastName()))
                .stream()
                .map(User::getId)
                .max(Comparator.naturalOrder())
                .orElse(null);

        if(Objects.nonNull(lastId)) {
            serialNumber = lastId.intValue() + 1;
        }

        User newUser = new User();
        newUser.setFirstName(request.getFirstName());
        newUser.setLastName(request.getLastName());
        newUser.setUsername(DomainUtil.generateUsername(request.getFirstName(), request.getLastName(), serialNumber));
        String password = DomainUtil.generatePassword();
        String encryptedPassword = encoder.encode(password);
        newUser.setPassword(encryptedPassword);

        Trainee newTrainee = new Trainee();
        newTrainee.setUser(newUser);
        newTrainee.setDateOfBirth(request.getDateOfBirth());
        newTrainee.setAddress(request.getAddress());

        Trainee savedTrainee = traineeRepository.save(newTrainee);

        return new RegistrationResponse(savedTrainee.getUser().getUsername(), password);
    }

    public TraineeDTO updateByUsername(String username, TraineeRequest traineeRequest) {
        Trainee trainee = this.getByUsername(username);
        trainee.setAddress(traineeRequest.getAddress());
        trainee.setDateOfBirth(traineeRequest.getDateOfBirth());
        trainee.getUser().setActive(traineeRequest.isActive());
        trainee.getUser().setFirstName(traineeRequest.getFirstName());
        trainee.getUser().setLastName(traineeRequest.getLastName());
        Trainee updatedTrainee = traineeRepository.save(trainee);
        return new TraineeDTO(updatedTrainee);
    }

    public List<TrainerListDTO> updateTrainers(String username, List<String> trainersUsername) {
        Trainee trainee = traineeRepository.findOneByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Trainee with username [%s] was not found", username)));

        Set<Trainer> trainers = trainerService.findAllByUsernameList(trainersUsername);
        trainee.setTrainers(trainers);

        return traineeRepository.save(trainee)
                .getTrainers()
                .stream()
                .map(TrainerListDTO::new)
                .collect(Collectors.toList());
    }

    public void deleteById(Long traineeId) {
        log.info("Deleting Trainee with ID: " + traineeId);
        traineeRepository.deleteById(traineeId);
        log.info("Trainee deleted with ID: " + traineeId);
    }

    public Trainee getById(Long traineeId) {
        log.info("Selecting Trainee with ID: " + traineeId);
        return traineeRepository.findById(traineeId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Trainee with id [%d] was not found", traineeId)));
    }

    public List<TETrainingListDTO> getTrainingsByCriteria(TrainingCriteria criteria) {
        Specification<Trainee> spec = Specification.where(null);

        if (criteria.getUsername() != null) {
            spec = spec.and(TraineeSpecifications.hasUsername(criteria.getUsername()));
        }

        if (criteria.getTraineeName() != null) {
            spec = spec.and(TraineeSpecifications.hasTrainerName(criteria.getTraineeName()));
        }

        if (criteria.getTrainingType() != null) {
            spec = spec.and(TraineeSpecifications.equalTrainingId(criteria.getTrainingType().getId()));
        }

        if (criteria.getFrom() != null) {
            spec = spec.and(TraineeSpecifications.greaterThan(criteria.getFrom()));
        }

        if (criteria.getTo() != null) {
            spec = spec.and(TraineeSpecifications.lessThan(criteria.getTo()));
        }

        var trainee = traineeRepository.findOne(spec).orElseThrow();

        return trainee
                .getTrainings()
                .stream()
                .map(training -> new TETrainingListDTO(training, trainee.getUser().getFirstName()))
                .collect(Collectors.toList());
    }

    public void updateActivation(String username, boolean isActive) {
        traineeRepository.findOneByUsername(username)
                .ifPresentOrElse(trainee -> {
                    trainee.getUser().setActive(isActive);
                    traineeRepository.save(trainee);
                }, () -> {
                    throw new EntityNotFoundException(String.format("Trainee with username [%s] was not found", username));});
    }

    public Trainee getByUsername(String username) {
        return traineeRepository.findOneByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Trainee with username [%s] was not found", username)));
    }

    public void deleteByUsername(String username) {
        traineeRepository.deleteByTraineeUsername(username);
    }
}
