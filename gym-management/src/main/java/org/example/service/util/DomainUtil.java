package org.example.service.util;

import lombok.experimental.UtilityClass;
import net.bytebuddy.utility.RandomString;
import org.springframework.stereotype.Component;

@Component
public class DomainUtil {

    public static String generateUsername(String firstName, String lastName, int serialNumber) {
        if (serialNumber == 0) {
            return String.join(".", firstName, lastName);
        }
        return String.join(".", firstName, lastName) + serialNumber;
    }

    public static String generatePassword() {
        return RandomString.make();
    }
}
