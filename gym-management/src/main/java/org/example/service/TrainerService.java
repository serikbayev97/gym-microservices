package org.example.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.TrainingMessage;
import org.example.criteria.TrainerSpecifications;
import org.example.domain.Trainer;
import org.example.domain.Training;
import org.example.domain.User;
import org.example.repository.TrainerRepository;
import org.example.rest.dto.RegistrationResponse;
import org.example.rest.dto.TrainerRegistrationRequest;
import org.example.rest.dto.TrainingCriteria;
import org.example.service.dto.TRTrainingListDTO;
import org.example.service.dto.TrainerDTO;
import org.example.service.dto.TrainerListDTO;
import org.example.service.dto.TrainerRequest;
import org.example.service.util.DomainUtil;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class TrainerService {

    private final TrainerRepository trainerRepository;
    private final UserService userService;
    private final PasswordEncoder encoder;

    public RegistrationResponse create(TrainerRegistrationRequest request) {
        log.info("Trainer registration request {}", request);
        int serialNumber = 0;

        Long lastId = userService.getAllByUsername(String.join(".", request.getFirstName(), request.getLastName()))
                .stream()
                .map(User::getId)
                .max(Comparator.naturalOrder())
                .orElse(null);

        if(Objects.nonNull(lastId)) {
            serialNumber = lastId.intValue() + 1;
        }

        User newUser = new User();
        newUser.setFirstName(request.getFirstName());
        newUser.setLastName(request.getLastName());
        newUser.setUsername(DomainUtil.generateUsername(request.getFirstName(), request.getLastName(), serialNumber));
        String password = DomainUtil.generatePassword();
        String encryptedPassword = encoder.encode(password);
        newUser.setPassword(encryptedPassword);

        Trainer newTrainer = new Trainer();
        newTrainer.setUser(newUser);
        newTrainer.setSpecialization(request.getSpecialization());

        Trainer savedTrainer = trainerRepository.save(newTrainer);

        return new RegistrationResponse(savedTrainer.getUser().getUsername(), password);
    }

    public TrainerDTO update(String username, TrainerRequest request) {
        Trainer trainer = this.getByUsername(username);
        trainer.setSpecialization(request.getSpecialization());
        trainer.getUser().setFirstName(request.getFirstName());
        trainer.getUser().setLastName(request.getLastName());
        Trainer updatedTrainer = trainerRepository.save(trainer);
        return new TrainerDTO(updatedTrainer);
    }

    public Trainer select(Long trainerId) {
        log.info("Selecting Trainer with ID: " + trainerId);
        return trainerRepository.findById(trainerId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Trainer with id [%d] was not found", trainerId)));
    }

    public Trainer getById(Long trainerId) {
        log.info("Selecting Trainer with ID: " + trainerId);
        return trainerRepository.findById(trainerId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Trainer with id [%d] was not found", trainerId)));
    }

    public List<TrainerListDTO> getAllNotAssignedActiveTrainers(String username) {
        return trainerRepository.findAll()
                .stream()
                .filter(trainer -> username.equals(trainer.getUser().getUsername()))
                .filter(trainer -> trainer.getTrainees().isEmpty())
                .filter(trainer -> trainer.getUser().isActive())
                .map(TrainerListDTO::new)
                .collect(Collectors.toList());

    }

    public Set<Trainer> findAllByUsernameList(List<String> usernames) {
        return trainerRepository.findAllByUsernamesIn(usernames);
    }

    public List<TRTrainingListDTO> getTrainingsByCriteria(TrainingCriteria criteria) {
        Specification<Trainer> spec = Specification.where(null);

        if (criteria.getUsername() != null) {
            spec = spec.and(TrainerSpecifications.hasUsername(criteria.getUsername()));
        }

        if (criteria.getTrainerName() != null) {
            spec = spec.and(TrainerSpecifications.hasTraineeName(criteria.getTrainerName()));
        }

        if (criteria.getFrom() != null) {
            spec = spec.and(TrainerSpecifications.greaterThan(criteria.getFrom()));
        }

        if (criteria.getTo() != null) {
            spec = spec.and(TrainerSpecifications.lessThan(criteria.getTo()));
        }

        var trainer = trainerRepository.findOne(spec).orElseThrow();

        return trainer.getTrainings()
                .stream()
                .map(training -> new TRTrainingListDTO(training, trainer.getUser().getFirstName()))
                .collect(Collectors.toList());

    }

    public void updateActivation(String username, boolean isActive) {
        trainerRepository.findOneByUsername(username)
                .ifPresentOrElse(trainer -> {
                    trainer.getUser().setActive(isActive);
                    trainerRepository.save(trainer);
                }, () ->
                { throw new EntityNotFoundException(String.format("Trainer with username [%s] was not found", username));});
    }

    public Trainer getByUsername(String username) {
        return trainerRepository.findOneByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Trainer with username [%s] was not found", username)));
    }

    public void deleteByUsername(String username) {
        trainerRepository.findOneByUsername(username)
                .ifPresent(trainerRepository::delete);
    }


    public void updateTrainings(Trainer trainer, TrainingMessage trainingMessage) {
        Training newTraining = new Training();
        newTraining.setName(trainingMessage.getTrainingName());
        newTraining.setDate(trainingMessage.getTrainingDate());
        newTraining.setDuration(trainingMessage.getDuration());
        newTraining.setTrainingType(trainer.getSpecialization());
        trainer.addTraining(newTraining);
        trainerRepository.save(trainer);
    }
}
