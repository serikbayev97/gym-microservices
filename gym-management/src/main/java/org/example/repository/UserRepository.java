package org.example.repository;

import org.example.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    boolean existsByUsernameAndPasswordEquals(String username, String password);
    Optional<User> findByUsername(String username);
    List<User> findAllByUsernameLike(String username);
}
