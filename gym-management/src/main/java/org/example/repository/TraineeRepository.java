package org.example.repository;

import org.example.domain.Trainee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TraineeRepository extends JpaRepository<Trainee, Long>, JpaSpecificationExecutor<Trainee> {
    @Query(value = "select t from Trainee t where t.user.username = :username")
    Optional<Trainee> findOneByUsername(@Param(value = "username") String username);

    @Modifying
    @Query(value = "delete from Trainee t where t.user.username = :username")
    void deleteByTraineeUsername(@Param(value = "username") String username);
}
