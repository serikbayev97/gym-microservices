package org.example.repository;

import org.example.domain.Trainee;
import org.example.domain.Trainer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface TrainerRepository extends JpaRepository<Trainer, Long>, JpaSpecificationExecutor<Trainer> {
    @Query(value = "select t from Trainer t where t.user.username = :username")
    Optional<Trainer> findOneByUsername(@Param(value = "username") String username);

    @Query(value = "select t from Trainer t where t.user.username in (:usernames)")
    Set<Trainer> findAllByUsernamesIn(@Param("usernames") List<String> usernames);
}
